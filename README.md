# Studying College Mathematics by Jim Hefferon

Slides for a video giving advice on how to study for beginning college
mathematics courses.  The goals are to conform to what is known by
experts in learning, and by math teachers, and to be brief. 

See https://youtu.be/KQ9288zYZLY .

## License
CC-BY-SA 4.0

## Project status
2024-Jun-16 Jim Hefferon  Initial